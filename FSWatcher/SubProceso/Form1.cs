﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace SubProceso
{
    public partial class Form1 : Form
    {
        Thread hora;

        Thread hilo;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("HOLA CLASE");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hilo = new Thread(new ThreadStart(Iterar ));

            hilo.Start();


        }

        void Iterar()
        {
            for (long i = -99999999999; i < 9999999999999; i++)
            {
                Mostrar(i);

            }
        }

        delegate void delMostrar(long numero);

        void Mostrar(long numero)
        {
            if (label1.InvokeRequired)
            {
                label1.Invoke(new delMostrar(Mostrar), numero);
            }
            else
            { 
                label1.Text = numero.ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            hilo.Abort();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            hora = new Thread(new ThreadStart(Horario));
            hora.Start();
        }

        void Horario()
        {
            while (true)
            {
                MostrarHora();
                Thread.Sleep(2000);
            }
        }

        delegate void delMostrarHora();

        void MostrarHora()
        {
            if (label2.InvokeRequired)
            {
                label2.Invoke(new delMostrarHora(MostrarHora));
            }
            else
            {
                label2.Text = DateTime.Now.ToLongTimeString() + "." + DateTime.Now.Millisecond.ToString();
            }

        }
    }
}
