﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
namespace FSWatcher
{
    public partial class Form1 : Form
    {
        FileSystemWatcher fs;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            fs.Path = @"D:\REVISAR\";

            fs.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName
                | NotifyFilters.CreationTime | NotifyFilters.Security | NotifyFilters.Attributes;

            fs.Created += Fs_Created;
            fs.Changed += Fs_Changed;
            fs.Renamed += Fs_Renamed;
            fs.Deleted += Fs_Deleted;
            //fs.BeginInit();

            fs.Filter = "*.*";

            fs.EnableRaisingEvents = true;
          
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            fs = new FileSystemWatcher();
        }

        private void Fs_Deleted(object sender, FileSystemEventArgs e)
        {
            //Cada vez que se borra un archivo
            Insertar("Borrado: " + e.Name);

        }

        private void Fs_Renamed(object sender, RenamedEventArgs e)
        {
            //Cada vez que se renombra el archivo
            Insertar("Renombrado: " + e.OldName + " x " + e.Name);
        }

        private void Fs_Changed(object sender, FileSystemEventArgs e)
        {
            //Cada vez que el archivo cambia
            Insertar("Cambio: " + e.Name);
        }

        private void Fs_Created(object sender, FileSystemEventArgs e)
        {
            //Cada vez se crea un archivo
           Insertar("Creación: " + e.Name);

           Thread.Sleep(1000);

            File.Copy(e.FullPath, @"d:\REVISAR\CARPETA\" + e.Name);

        }



        private void button2_Click(object sender, EventArgs e)
        {
            fs.EndInit();
        }

        delegate void delInsertar(string texto);

        void Insertar(string texto)
        {
            if (listBox1.InvokeRequired)
            {
                listBox1.Invoke(new delInsertar(Insertar), texto);
            }
            else
            {
                listBox1.Items.Insert(0, texto);
            }
        
        }


    }
}
