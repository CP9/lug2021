﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;

namespace Procesos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            listBox1.Items.Clear();


            foreach (Process p in (from Process pr in Process.GetProcesses()
                                   orderby pr.ProcessName
                                   select pr).ToList())
            {
                listBox1.Items.Add(p);

            }

            listBox1.DisplayMember = "ProcessName";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Enlazar();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process p = (Process)listBox1.SelectedItem;

            p.Kill();
            Enlazar();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Process p = (Process)listBox1.SelectedItem;

            label1.Text = p.ProcessName;

            label2.Text = p.Id.ToString();

            try
            {
                label3.Text = p.MainWindowTitle;
            }
            catch
            {
                label3.Text = "N/A";
            }

            try
            {
                label4.Text = p.StartTime.ToString();
            }
            catch
            {
                label4.Text = "N/A";           
                
            }
            try
            {
                   label5.Text = p.MainWindowHandle.ToString();
            
            }
            catch
            {
                label5.Text = "N/A";

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
#if ! DEBUG

            this.Text += " MODO DEBUG";
#endif



        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process p = (Process)listBox1.SelectedItem;

            Graphics g = Graphics.FromHwnd(p.MainWindowHandle);

            g.Clear(Color.Yellow);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process.Start(textBox1.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start(textBox1.Text, textBox2.Text);
        }
    }
}
