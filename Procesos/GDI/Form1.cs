﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI
{
    public partial class Form1 : Form
    {
        Graphics graficador;

        Graphics graficadorImg; 

        List<Estrella> estrellas = new List<Estrella>();

        Bitmap bmp;

        Image imagen;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Formularios, Imagenes, Controles

            // graficador = this.CreateGraphics();

            for (int i = 0; i < 10; i++)
            {
                Point p = new Point(Helper.rnd.Next(10, this.Size.Width), Helper.rnd.Next(10, this.Size.Height));

                Estrella estrella = new Estrella();

                estrella.Punto = p;

                estrellas.Add(estrella);
            }

            bmp = new Bitmap(this.Size.Width, this.Size.Height);

            imagen = Image.FromHbitmap(bmp.GetHbitmap());

            graficadorImg = Graphics.FromImage(imagen);
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            graficador = Graphics.FromHwnd(this.Handle);

            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            graficadorImg.Clear(Color.Black);

            foreach (Estrella estrella in estrellas)
            {
                estrella.Actualizar();
                estrella.Dibujar(graficadorImg);
            }

            imagen.Save(@"D:\LUG\" + DateTime.Now.Ticks.ToString() + ".jpg");

            graficador.DrawImage(imagen, new Point(0, 0));
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
        }
    }
}
