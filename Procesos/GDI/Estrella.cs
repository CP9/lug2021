﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GDI
{
    class Estrella
    {

        public Estrella()
        {
            switch (Helper.rnd.Next(0, 3))
            {
                case 0:
                    {
                        color = Brushes.White;
                        break;
                    }
                case 1:
                    {
                        color = Brushes.Yellow;
                        break;
                    }
                case 2:
                    {
                        color = Brushes.Red;
                        break;
                    }
            }

            tamaño = Helper.rnd.Next(20, 50);
            tamañoMaximo = tamaño;
        }

        private Brush color;

        private int tamaño;

        public int tamañoMaximo;

        public int valor = -1;

        private Point punto;

        public Point Punto
        {
            get { return punto; }
            set { punto = value; }
        }


        public void Actualizar()
        {
            if (tamaño == 0)
            {
                valor = 1;
            }
            else if (tamaño== tamañoMaximo)
            {
                valor = -1;
            }

            tamaño += valor;
        }

        public void Dibujar(Graphics graficador)
        {

            Rectangle rect = new Rectangle(punto, new Size(tamaño, tamaño));

            graficador.FillEllipse(color,rect ); 
        
        }


    }
}
