﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace LUG_XML2
{
    public partial class Form1 : Form
    {
        XmlDocument documento = new XmlDocument();
        XmlNode nodoActual;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            documento.Load(@"D:\Carreras.xml");
            nodoActual = documento.FirstChild;
            MostrarNodo();
        }

        void MostrarNodo()
        {
            label1.Text = nodoActual.Name;
            label2.Text = nodoActual.NodeType.ToString();
            label3.Text = nodoActual.Value;
            label4.Text = nodoActual.InnerText;
            label5.Text = nodoActual.InnerXml;
            label6.Text = "";
            if (nodoActual.Attributes != null)
            { 
                foreach (XmlAttribute atributo in nodoActual.Attributes)
                {
                    label6.Text += " " + atributo.Name + ": " + atributo.Value;
                
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (nodoActual.NextSibling != null)
            {
                nodoActual = nodoActual.NextSibling;
                MostrarNodo();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (nodoActual.PreviousSibling != null)
            {
                nodoActual = nodoActual.PreviousSibling;
                MostrarNodo();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (nodoActual.HasChildNodes)
            {
                nodoActual = nodoActual.FirstChild;
                MostrarNodo();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (nodoActual.LastChild != null)
            {
                nodoActual = nodoActual.LastChild;
                MostrarNodo();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (nodoActual.ParentNode != null)
            {
                nodoActual = nodoActual.ParentNode;
                MostrarNodo();
            }
        }
    }
}
