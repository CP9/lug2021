﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace LUG_XML
{
    public partial class Form1 : Form
    {
        DataSet ds = new DataSet();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        void Enlazar()
        {
            dataGridView1.DataSource = null;
            if (File.Exists("D:\\esquema.xml"))
            {
                ds.ReadXmlSchema("D:\\esquema.xml");
            }


            if (ds.Tables.Count == 0 && !File.Exists("D:\\esquema.xml"))
           
            {
                DataTable tabla = new DataTable("CARRERA");
                tabla.Columns.Add("NOMBRE");
                tabla.Columns.Add("DURACION");
                ds.Tables.Add(tabla);
                ds.WriteXmlSchema("D:\\esquema.xml");
            }
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataRow registro = ds.Tables[0].NewRow();
            registro["NOMBRE"] = textBox1.Text;
            registro["DURACION"] = textBox2.Text;

            ds.Tables[0].Rows.Add(registro);

            ds.WriteXml(@"d:\carreras.xml");
            Enlazar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ds.Tables[0].Rows.Clear();
            ds.WriteXml(@"d:\carreras.xml");
            Enlazar();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ds.Tables.Clear();
            ds.ReadXml(@"d:\Carreras.xml");

            Enlazar();
        }
    }
}
