﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Lug_Clase1
{
    class ACCESO
    {
        private SqlConnection conexion;


        public ACCESO()
        {
            conexion = new SqlConnection();

            conexion.ConnectionString = @"Data source=.; Initial Catalog=CLASE1_LUG; Integrated Security = SSPI ";

        }

        ~ACCESO()
        {
            conexion = null;
        }

        private void Abrir()
        {
           
            conexion.Open();
        }

        private void Cerrar()
        {
            conexion.Close();
                  
        }

        public int Escribir(string sql)
        {
            Abrir();

            SqlCommand comando = new SqlCommand();

            comando.CommandType = CommandType.Text;

            comando.Connection = conexion;

            comando.CommandText = sql;

            int filasAfectadas = comando.ExecuteNonQuery();


            Cerrar();

            return filasAfectadas;
        }


        public List<PERSONA> Leer()
        {
            List<PERSONA> personas = new List<PERSONA>();
            Abrir();
            SqlCommand comando = new SqlCommand();
            comando.CommandType = CommandType.Text;
            comando.Connection = conexion;
            comando.CommandText = "SELECT ID,NOMBRE,APELLIDO,EDAD FROM PERSONA";

            SqlDataReader lector = comando.ExecuteReader() ;

            while (lector.Read())
            {
                PERSONA p = new PERSONA();
                p.Id = int.Parse(lector[0].ToString());
                p.Nombre = lector.GetString(1);

                p.Apellido = lector["APELLIDO"].ToString();

                p.Edad = int.Parse(lector["EDAD"].ToString());
                personas.Add(p);
            }

            lector.Close();
            Cerrar();
            return personas;
        }


    }
}
