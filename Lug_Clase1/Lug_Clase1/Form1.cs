﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lug_Clase1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            dataGridView1.DataSource = null;

            dataGridView1.DataSource = acceso.Leer();

            acceso = null;
            GC.Collect();
        }


        private void Escribir(string sql)
        {
            ACCESO acceso = new ACCESO();

            acceso.Escribir(sql);

            acceso = null;
            GC.Collect();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO PERSONA (NOMBRE, APELLIDO, EDAD) VALUES ('" + txtNombre.Text + "','" +txtApellido.Text +"'," + txtEdad.Text +")"  ;
            Escribir(sql);
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string sql = " update persona set nombre = '" + txtNombre.Text + "', apellido='" + txtApellido.Text + "', edad = " + txtEdad.Text + " where id=" +lblID.Text;

            Escribir(sql);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sql = " delete from persona where id=" + lblID.Text;

            Escribir(sql);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PERSONA p = (PERSONA)dataGridView1.Rows[e.RowIndex].DataBoundItem;
            lblID.Text = p.Id.ToString();
            txtApellido.Text = p.Apellido;
            txtNombre.Text = p.Nombre;
            txtEdad.Text = p.Edad.ToString();

        }
    }
}
