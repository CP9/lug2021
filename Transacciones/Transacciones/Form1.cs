﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Transacciones
{
    public partial class Form1 : Form
    {


        Acceso acceso = new Acceso();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", textBox1.Text));

            parametros.Add(acceso.CrearParametro("@edad", int.Parse(textBox2.Text)));


            acceso.Escribir("Alumno_Insertar",parametros);

            Enlazar();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        void Enlazar()
        {
            acceso.Abrir();
            Grilla.DataSource = null;
            Grilla.DataSource = acceso.Leer("Alumno_listar");
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            acceso.Abrir();
            acceso.IniciarTransaccion();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            acceso.ConfirmarTransaccion();
            acceso.Cerrar();
            Enlazar();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            acceso.DeshacerTransaccion();
            acceso.Cerrar();
            Enlazar();
        }
    }
}
