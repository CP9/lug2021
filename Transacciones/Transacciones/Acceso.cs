﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace Transacciones
{
    class Acceso
    {
        SqlConnection conexion;

        SqlTransaction transaccion;

        public void Abrir()
        {
            if (conexion == null || conexion.State != ConnectionState.Open)
            {
                conexion = new SqlConnection();
                conexion.ConnectionString = "initial catalog=lug_clase4; Integrated Security=SSPI; Data Source=.";
                conexion.Open();
            }
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion.Dispose();
            conexion = null;
            GC.Collect();
        }


        public void IniciarTransaccion()
        {
            if(conexion!=null && conexion.State == ConnectionState.Open)
            { 
                transaccion = conexion.BeginTransaction();
            }
        }

        public void ConfirmarTransaccion()
        {
            transaccion.Commit();

        }

        public void DeshacerTransaccion()
        {
            transaccion.Rollback();
            
        }

        private SqlCommand CrearComando(string nombre, List<SqlParameter> parametros = null)
        {
            SqlCommand comando = new SqlCommand();
            comando.CommandText = nombre;
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;

            if (transaccion != null)
            {
                comando.Transaction = transaccion;
            }

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;        
        }

        public int Escribir(string nombre, List<SqlParameter> parametros = null)
        {
            int filasafectadas = 0;

            if (conexion != null && conexion.State == ConnectionState.Open)
            {
                SqlCommand comando = CrearComando(nombre, parametros);
                try
                {
                    filasafectadas = comando.ExecuteNonQuery();
                }
                catch
                {
                    filasafectadas = -1;
                }
                comando.Parameters.Clear();
                comando.Dispose();
                comando = null;
            }
            else
            {
                filasafectadas = -2;
            }
            return filasafectadas;
        }

        public DataTable Leer(string nombre, List<SqlParameter> parametros = null)
        {
            DataTable tabla = new DataTable();
            if (conexion != null && conexion.State == ConnectionState.Open)
            {
                using (SqlDataAdapter adaptador = new SqlDataAdapter())
                {
                    adaptador.SelectCommand = CrearComando(nombre, parametros);
                    adaptador.Fill(tabla);
                    adaptador.Dispose();
                }
            }
            return tabla;        
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.String;
            return parametro;
            
        }
        public SqlParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parametro = new SqlParameter(nombre, valor);
            parametro.DbType = DbType.Int32;
            return parametro;

        }






    }
}
