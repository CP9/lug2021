﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Color
    {
        public List<BE.Color> Listar()
        {
            DAL.MP_COLOR mp = new DAL.MP_COLOR();

            return mp.Listar();        
        }


        public int Grabar(BE.Color color)
        {
            int res = 0;
            DAL.MP_COLOR mp_color = new DAL.MP_COLOR();
            if (color.Id == 0)
            {
                res = mp_color.Insertar(color);
            }
            else
            {
                res = mp_color.Editar(color);
            }
            return res;
        }

        public int Eliminar(BE.Color color)
        {
            int res = 0;
            DAL.MP_COLOR mp_color = new DAL.MP_COLOR();
            res = mp_color.Borrar(color);
            return res;
        }


    }
}
