﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MP_COLOR: MAPPER<BE.Color>
    {
        AccesoDatos acceso = new AccesoDatos();

        public override int Borrar(Color obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(acceso.CrearParametro("@id", obj.Id));

            return acceso.Escribir("color_borrar", parameters);

        }

        public override int Editar(Color obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(acceso.CrearParametro("@id", obj.Id));
            parameters.Add(acceso.CrearParametro("@color", obj.Nombre));

            return acceso.Escribir("color_editar", parameters);
        }

        public override int Insertar(Color obj)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

   
            parameters.Add(acceso.CrearParametro("@color", obj.Nombre));

            return acceso.Escribir("color_insertar", parameters);
        }

        public List<BE.Color> Listar()
        {
            List<BE.Color> colores = new List<BE.Color>();
    

            DataTable tabla = acceso.Leer("LISTAR_COLOR", null);
            
            foreach (DataRow registro in tabla.Rows)
            {
                BE.Color color = new BE.Color();

                color.Id =int.Parse( registro["Id_color"].ToString());
                color.Nombre = registro["nombre"].ToString();
                colores.Add(color);            
            }
            return colores;
        }


    }
}
