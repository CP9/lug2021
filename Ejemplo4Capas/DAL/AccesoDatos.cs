﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    internal class AccesoDatos
    {
        public delegate void delErrorEnSP(int numero);

        public delErrorEnSP ErrorEnSP;

        SqlConnection conexion = new SqlConnection();

        void Abrir()
        {
            conexion = new SqlConnection();
            conexion.ConnectionString = @"Initial Catalog=LUG_Clase3; Data Source=.; Integrated Security=SSPI;";
            conexion.Open();

        }

        void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();

        }

        private SqlCommand CrearComando(string nombre, List<SqlParameter> parametros)
        {
            SqlCommand comando = new SqlCommand();

            comando.CommandText = nombre;

            comando.Connection = conexion;

            comando.CommandType = CommandType.StoredProcedure;

            if (parametros != null && parametros.Count > 0)
            {
                foreach (SqlParameter p in parametros)
                {
                    comando.Parameters.Add(p);
                }
                //comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }

        public SqlParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parametro = new SqlParameter();

            parametro.ParameterName = nombre;
            parametro.Value = valor;
            parametro.DbType = DbType.String;

            return parametro;
        }
        public SqlParameter CrearParametro(string nombre, int valor, bool output = false)
        {
            SqlParameter parametro = new SqlParameter();

            parametro.ParameterName = nombre;
            parametro.Value = valor;

            if (output)
            {
                parametro.Direction = ParameterDirection.Output;
            }

            parametro.DbType = DbType.Int32;

            return parametro;
        }

        public IDataParameter CrearParametro(string nombre, long valor)
        {
            IDataParameter parametro = new SqlParameter();

            parametro.ParameterName = nombre;
            parametro.Value = valor;
            parametro.DbType = DbType.Int64;

            return parametro;
        }

        public SqlParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter parametro = CrearParametro(nombre, valor, DbType.DateTime);
            return parametro;
        }

        private SqlParameter CrearParametro(string nombre, object valor, DbType tipo)
        {
            SqlParameter parametro = new SqlParameter();
            parametro.Value = valor;
            parametro.ParameterName = nombre;
            parametro.DbType = tipo;
            return parametro;
        }

        public int Escribir(string nombre, List<SqlParameter> parametros = null)
        {
            Abrir();
            int filas = 0;

            SqlCommand comando = CrearComando(nombre, parametros);

            try
            {
                filas = comando.ExecuteNonQuery();

                SqlParameter parametro = (from SqlParameter par in comando.Parameters
                                          where par.Direction == ParameterDirection.Output
                                          select par).FirstOrDefault();

                if (parametro != null)
                {

                    ErrorEnSP(int.Parse(parametro.Value.ToString()));
                }

            }
            catch(Exception ex)
            {
                filas = -2;
            }

            Cerrar();

            return filas;
        }


        public DataTable Leer(string nombre, List<SqlParameter> parameters = null)
        {
            DataTable tabla = new DataTable();
            Abrir();

            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(nombre, parameters);

            adaptador.Fill(tabla);

            Cerrar();
            return tabla;
        }

    }
}
