﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Auto
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private Color color;

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }


    }
}
