﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo4Capas
{
    public partial class Form1 : Form
    {
        BLL.Color negColor = new BLL.Color();
       
        public Form1()
        {
            InitializeComponent();
        }

        void Enlazar()
        {
            grilla.DataSource = null;
            grilla.DataSource = negColor.Listar();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
            Grabar();
        }

        private void grilla_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Color color = (BE.Color)grilla.Rows[e.RowIndex].DataBoundItem;

            textBox1.Text = color.Id.ToString();
            textBox2.Text = color.Nombre;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Grabar();
        }

        void Grabar()
        {
            BE.Color color = new BE.Color();
            color.Nombre = textBox2.Text;
            color.Id = int.Parse(textBox1.Text);

            negColor.Grabar(color);
            Enlazar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Color color = new BE.Color();
            color.Id = int.Parse(textBox1.Text);

            negColor.Eliminar(color);
            Enlazar();
        }
    }
}
