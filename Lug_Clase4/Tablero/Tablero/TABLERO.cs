﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tablero
{
    class TABLERO
    {

        public delegate void delCrearCasillero(COORDENADA coordenada);

        public event delCrearCasillero CrearCasillero;

        private List<COORDENADA> tablero = new List<COORDENADA>();

        public List<COORDENADA> Tablero
        {
            get { return tablero; }
            set { tablero = value; }
        }

        public void CrearTablero()
        {
            tablero.Clear();

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    COORDENADA coordenada = new COORDENADA();
                    coordenada.X = x;
                    coordenada.Y = y;
                    coordenada.Valor = string.Empty;
                    tablero.Add(coordenada);
                    CrearCasillero(coordenada);
                }
                
            }        
        }

        public bool VerificarGanador(string valor)
        {
            bool ganador = false;

            List<COORDENADA> coordenadas = (from COORDENADA c in tablero
                                           where !string.IsNullOrWhiteSpace(c.Valor)
                                           select c).ToList();

            if (coordenadas.Count >= 5)
            {
                List<COORDENADA> lista = (from COORDENADA c in coordenadas
                                      where c.Valor == valor
                                      select c).ToList();

                ganador = VerificarHorizontal(lista) || VerificarVertical(lista) || VerificarDiagonal(lista);
            }

            return ganador; 
        }


        private bool VerificarHorizontal(List<COORDENADA> coordenadas)
        {
            bool ok = false;

            ok = ((from COORDENADA c in coordenadas
                  where c.Y == 0
                  select c).Count() == 3   ||
                  (from COORDENADA c in coordenadas
                   where c.Y == 1
                   select c).Count() == 3  ||
                  (from COORDENADA c in coordenadas
                   where c.Y == 2
                   select c).Count() == 3 );

            return ok;
        }

        private bool VerificarVertical(List<COORDENADA> coordenadas)
        {
            bool ok = false;

            ok = ((from COORDENADA c in coordenadas
                   where c.X == 0
                   select c).Count() == 3 ||
                  (from COORDENADA c in coordenadas
                   where c.X == 1
                   select c).Count() == 3 ||
                  (from COORDENADA c in coordenadas
                   where c.X == 2
                   select c).Count() == 3);

            return ok;
        }
        public bool VerificarDiagonal(List<COORDENADA> coordenadas)
        {
            bool ok = false;

            ok = ((from COORDENADA c in coordenadas
                  where (c.X == 0 && c.Y == 0) || (c.X == 1 && c.Y == 1) || (c.X == 2 && c.Y == 2)
                  select c).Count() == 3
                  ||
                  (from COORDENADA c in coordenadas
                   where (c.X == 0 && c.Y == 2) || (c.X == 1 && c.Y == 1) || (c.X == 2 && c.Y == 0)
                   select c).Count() == 3

                  );

            return ok;
            
        }

    }
}
