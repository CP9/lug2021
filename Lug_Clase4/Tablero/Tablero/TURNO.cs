﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tablero
{
    class TURNO
    {
        static TURNO()
        {
            nombre = "X";
        }

        private static string nombre;

        public  static string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public static void Cambiar()
        {
            nombre = (nombre == "X") ? "O" : "X";
            
        }

    }
}
