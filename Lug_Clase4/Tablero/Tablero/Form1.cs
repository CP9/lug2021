﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing;
using System.Drawing.Drawing2D;


namespace Tablero
{
    public partial class Form1 : Form
    {

        DataSet ds = new DataSet();

        DataTable tabla = new DataTable();

        TABLERO tablero = new TABLERO();
        public Form1()
        {
            InitializeComponent();


            tabla.Columns.Add(new DataColumn("Nro_Partida"));

            tabla.Columns.Add(new DataColumn("Resultado"));


            ds.Tables.Add(tabla);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tablero.CrearCasillero += Tablero_CrearCasillero;
            tablero.CrearTablero();

            label1.Text = "Turno: " + TURNO.Nombre;
        }

        private void Tablero_CrearCasillero(COORDENADA coordenada)
        {
            MiBoton casillero = new MiBoton();

            casillero.Text = "";

            casillero.Size = new Size(100, 100);

            casillero.Location = new Point(coordenada.X * 100 + 10, coordenada.Y * 100 + 10);

            casillero.Coordenada = coordenada;

            casillero.Click += Casillero_Click;

            this.Controls.Add(casillero);

        }

        private void Casillero_Click(object sender, EventArgs e)
        {
            //MiBoton boton = (MiBoton)sender;

            ((MiBoton)sender).Text = TURNO.Nombre;

            ((MiBoton)sender).Enabled = false;

            ((MiBoton)sender).Coordenada.Valor = TURNO.Nombre;

            if (tablero.VerificarGanador(TURNO.Nombre))
            {
                label2.Text = TURNO.Nombre + " GANA";

                DataRow registro = tabla.NewRow();

                registro["Nro_Partida"] = tabla.Rows.Count +1;

                registro["Resultado"] = label2.Text;


                tabla.Rows.Add(registro);

                foreach (Control c in this.Controls)
                {
                    if (c is MiBoton)
                    {
                        c.Enabled = false;
                    }    
                }

            }
            else
            {

                int cantidad = (from Control c in this.Controls
                                where c is MiBoton && c.Enabled
                                select c).Count();


                if (cantidad > 0)
                {
                    label2.Text = "";
                    TURNO.Cambiar();
                    label1.Text = "Turno: " + TURNO.Nombre;
                }
                else
                { 
                    label2.Text = "Partida empatada";

                    DataRow registro = tabla.NewRow();
                    registro["Nro_Partida"] = tabla.Rows.Count + 1;

                    registro["Resultado"] = label2.Text;

                    tabla.Rows.Add(registro);

                }
            }      


        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                if (control is MiBoton)
                {
                    ((MiBoton)control).Enabled = true;
                    ((MiBoton)control).Text = "";
                    ((MiBoton)control).Coordenada.Valor = string.Empty;
                }
            
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmResultados frm = new frmResultados();

            frm.Enlazar(tabla);

            frm.ShowDialog();
        }
    }
}
