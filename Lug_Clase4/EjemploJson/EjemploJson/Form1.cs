﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace EjemploJson
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            StreamReader lector = new StreamReader("jsonlog.txt");

            string json = lector.ReadToEnd();

            lector.Dispose();

            OBJETO obj=  JsonConvert.DeserializeObject<OBJETO>(json);

            obj.ConvertData();

            ;

        }
    }
}
