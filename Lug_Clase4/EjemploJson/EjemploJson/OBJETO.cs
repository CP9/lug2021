﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EjemploJson
{
    class OBJETO
    {
        public string region_code;
        public string ip;
        public string postal_code;
        public string country_code;
        public string city;
        public string dma_code;
        public string last_update;
        public string latitude;
        public string country_name;

        public string org;

        [JsonProperty("data")]
        private dynamic datajson;


        private Data[] data;
        public Data[] Data
        {
            get { return data; }
            set { data = value; }
        }

        public void ConvertData()
        {
            string datos = datajson.ToString();

            data = JsonConvert.DeserializeObject<Data[]>(datos);


        }


    }
}
