﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BLL.Jugador gestor = new BLL.Jugador();
            BE.Jugador jugador = new BE.Jugador();

            jugador.Nombre = textBox1.Text;
            jugador.Contraseña = textBox2.Text;

            jugador = gestor.Login(jugador);

            if (jugador is null)
            {
                MessageBox.Show("ERROR");
            }
            else
            {
                jugadorLogueado = jugador;
                this.Close();
            }
        }

        private BE.Jugador jugadorLogueado;

        public BE.Jugador Jugador
        {
            get { return jugadorLogueado; }
            set { jugadorLogueado = value; }
        }


        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
