﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class Form1 : Form
    {
        public static BE.Juego juego = new BE.Juego();

        BLL.Juego gestorJuego = new BLL.Juego();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.ShowDialog();

            if (frm.Jugador != null)
            {
                if (!gestorJuego.AgregarJugador(frm.Jugador, juego))
                {
                    MessageBox.Show("El jugador se encuentra logueado");
                }
            }
            listBox1.DataSource = null;
            listBox1.DataSource = juego.Jugadores;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!gestorJuego.Iniciar(juego))
            {
                MessageBox.Show("Faltan jugadores");
            }
        }
    }
}
