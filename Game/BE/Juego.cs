﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Juego
    {
        private List<Jugador> jugadores = new List<Jugador>();

        public List<Jugador> Jugadores
        {
            get { return jugadores; }
        }

        private int minimo = 2;

        public int Minimo 
        {
            get { return minimo ; }
            set { minimo = value; }
        }

        private bool iniciado;
        public bool Iniciado
        {
            get { return iniciado; }
            set { iniciado = value; }
        
        }

    }
}