﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Jugador:IComparable
    {


        public Jugador()
        { }


        public Jugador(string u, string c)
        {
            this.contraseña = c;
            this.nombre = u;
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }

        public override string ToString()
        {
            return nombre;
        }

        public int CompareTo(object obj)
        {
            if (obj is null)
            {
                return 1;
            }
            else 
            {
                Jugador j = (Jugador)obj;

                return (this.nombre.CompareTo(j.nombre) == 0 && this.contraseña.CompareTo(j.contraseña) == 0)? 0 : 1 ;
            
            }

        }
    }
}
