﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        private static List<BE.Jugador> jugadoresAlmacenados = new List<BE.Jugador>();

        static Jugador()
        { 
            jugadoresAlmacenados.Add(new BE.Jugador("pepe","argento"));
            jugadoresAlmacenados.Add(new BE.Jugador("juan","perez"));
            jugadoresAlmacenados.Add(new BE.Jugador("pablo","marmol"));
            jugadoresAlmacenados.Add(new BE.Jugador("mario", "bros"));
        
        }

        
        
        
        public BE.Jugador Login(BE.Jugador jugador)
        {
            BE.Jugador j = (from BE.Jugador jg in jugadoresAlmacenados
                            where jg.Nombre == jugador.Nombre && jg.Contraseña == jugador.Contraseña
                            select jg).FirstOrDefault();

            return j;
        
        }
        

    }
}
