﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Juego
    {
        public bool AgregarJugador(BE.Jugador jugador, BE.Juego juego)
        {
            bool ok = false;
            if(!juego.Jugadores.Exists(x=> x.Nombre == jugador.Nombre ) )
            {
                juego.Jugadores.Add(jugador);
                ok = true;
            }
            return ok;
        }


        public bool Iniciar(BE.Juego juego)
        {
            bool ok = false;
            if (juego.Jugadores.Count >= juego.Minimo)
            {
                juego.Iniciado = true;
                ok = true;
                
            }
            return ok;

        
        }

    }
}
