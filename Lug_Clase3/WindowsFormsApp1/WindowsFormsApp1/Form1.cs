﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        ACCESO acceso = new ACCESO();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add(new COLOR("Azul",1));
            comboBox1.Items.Add(new COLOR("Amarillo",2));
            comboBox1.Items.Add(new COLOR("Rojo",3));

            comboBox1.SelectedIndex = 0;
            Enlazar();
        }

        void ErrorEnSp(int numero)
        {
            if (numero == 3030)
            {
                label4.Text = "Patente Repetida";            
            }        
        }

        public void Enlazar()
        {
            DataTable tabla = acceso.Leer("Listar");
            List<AUTO> autos = new List<AUTO>();

            foreach (DataRow registro in tabla.Rows)
            {
                AUTO auto = new AUTO();
                auto.ID = int.Parse(registro["ID_AUTO"].ToString());

                auto.Patente = registro["PATENTE"].ToString();

                auto.Color = (from COLOR color in comboBox1.Items
                              where color.ID == int.Parse(registro["ID_color"].ToString())
                              select color).First();
                autos.Add(auto);
            }
            Grilla.DataSource = null;
            Grilla.DataSource = autos;

        }


        private void button1_Click(object sender, EventArgs e)
        {
         

            acceso.ErrorEnSP +=  ErrorEnSp;

            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(acceso.CrearParametro("@patente", textBox1.Text));

            COLOR color = (COLOR)comboBox1.SelectedItem;

            parametros.Add(acceso.CrearParametro("@id_color", color.ID));

            parametros.Add(acceso.CrearParametro( "@error", 0, true));

            int res = acceso.Escribir( "AUTO_INSERTAR" ,parametros );

            switch (res)
            {
                case 1:
                    {
                        label3.Text = "OK";
                        break;
                    }
                case -1:
                    {

                        label3.Text = "NO ESCRIBIÓ PERO NO PINCHÓ";
                        break;
                    }
                default:
                    {
                        label3.Text = "EXPLOTÓ";
                        break;
                    }
            }
            Enlazar();
        }
    }
}
