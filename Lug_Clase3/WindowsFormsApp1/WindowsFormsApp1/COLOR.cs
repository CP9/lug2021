﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class COLOR
    {
        public COLOR()
        { 
        }

        public COLOR(string nombrex, int idx)
        {
            this.id = idx;
            this.nombre = nombrex;
        }

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public override string ToString()
        {
            return nombre;
        }

    }
}
