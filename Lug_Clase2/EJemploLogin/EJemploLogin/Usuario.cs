﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EJemploLogin
{
    class Usuario
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string usuario;

        public string Username
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }

        private int intentos;

        public int Intentos
        {
            get { return intentos; }
            set { intentos = value; }
        }

        private bool bloqueado;

        public  bool Bloqueado
        {
            get { return bloqueado; }
            set { bloqueado = value; }
        }

    }
}
