﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EJemploLogin
{
    public partial class frmRegistro : Form
    {
        ACCESO acceso = new ACCESO();
        public frmRegistro()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            SqlParameter parametro = new SqlParameter();
            parametro.ParameterName = "@usu";
            parametro.Value = textBox1.Text;
            parametros.Add(parametro);

            int cantidad = acceso.LeerEscalar("Select count(*) from usuario where usuario =@usu",parametros);

            bool existe = (cantidad == 1);



            if (!existe)
            {

                SqlParameter parametro2 = new SqlParameter();
                parametro2.ParameterName = "@pass";
                parametro2.Value = textBox2.Text;

                SqlParameter parametroId = new SqlParameter();
                parametroId.ParameterName = "@id";
                parametroId.Value = acceso.LeerEscalar("Select isnull(max(id),0)+1 from usuario");

                parametros.Add(parametro2);
                parametros.Add(parametroId);

                string sql = "Insert into Usuario(id, usuario, contraseña,bloqueado, intentos) values (@id,@usu,@pass,0,0)";

                acceso.Escribir(sql, parametros);
                this.Close();
            }
            else
            {
                MessageBox.Show("El usuario ya existe");
            }





        }
    }
}
