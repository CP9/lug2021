﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace EJemploLogin
{
    public partial class frmLogin : Form
    {

        public frmLogin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter parametro = new SqlParameter();
            parametro.Value = textBox1.Text;
            parametro.ParameterName = "@usu";
            parametro.DbType = DbType.String;

            parametros.Add(parametro);

            parametro = new SqlParameter();
            parametro.Value = textBox2.Text;
            parametro.ParameterName = "@pass";
            parametro.DbType = DbType.String;
            parametros.Add(parametro);

            string sql = "select * from usuario where usuario= @usu and contraseña = @pass";
            Usuario usuario = acceso.Login(sql,parametros);

            if (usuario == null)
            {
                MessageBox.Show("USUARIO INCORRECTO");

                sql = "update usuario set intentos+=1 where usuario= @usu and intentos <3";

                parametros.RemoveAt(1);

                int filas= acceso.Escribir(sql, parametros);

                if (filas == 1)
                {
                    sql = "Select intentos from usuario where usuario = @usu";
                    int cantidad = acceso.LeerEscalar( sql, parametros );

                    if (cantidad == 3)
                    {
                        sql = "Update usuario set bloqueado =@bloqueo where usuario = @usu";

                        parametro = new SqlParameter();
                        parametro.DbType = DbType.Int32;
                        parametro.Value = 1;
                        parametro.ParameterName = "@bloqueo";
                        parametros.Add(parametro);

                        if(acceso.Escribir(sql, parametros) == 1) 
                        {
                            MessageBox.Show("Se ha bloqueado el usuario");
                        }
                    }
                }

            }
            else
            {
                if (usuario.Bloqueado)
                {
                    MessageBox.Show("USUARIO Bloqueado");
                }
                else
                {
                    MessageBox.Show("BIENVENIDO " + usuario.Username);
                    
                    parametros.RemoveAt(1);
                    
                    sql = "Update usuario set intentos =@intentos where usuario = @usu";

                    parametro = new SqlParameter();
                    parametro.DbType = DbType.Int32;
                    parametro.Value = 0;
                    parametro.ParameterName = "@intentos";
                    parametros.Add(parametro);

                    acceso.Escribir(sql, parametros);

                }
            }
        }

        private void lnkRegistro_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmRegistro frm = new frmRegistro();
            frm.ShowDialog();

        }
    }
}
