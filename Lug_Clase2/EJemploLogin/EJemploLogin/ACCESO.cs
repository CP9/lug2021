﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace EJemploLogin
{
    class ACCESO
    {
        SqlConnection conexion = new SqlConnection();

        void Abrir()
        {
            conexion.ConnectionString = "Initial Catalog=Clase2_lug; Data source=.; Integrated Security=SSPI ";
            conexion.Open();
        }

        void Cerrar()
        {
            conexion.Close();
            GC.Collect();
        }

        private SqlCommand CrearComando(string sql, List<SqlParameter> parametros )
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;

            comando.CommandType = CommandType.Text;
            comando.CommandText = sql;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            
            }


            return comando;        
        }

        public Usuario Login(string sql, List<SqlParameter> parametros )
        {
            Usuario usuario = null;
            Abrir();

            SqlCommand comando = CrearComando(sql, parametros );

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                usuario = new Usuario();

                usuario.Id = lector.GetInt32(0);
                usuario.Username = lector.GetString(1);
                usuario.Contraseña = lector.GetString(2);
                usuario.Bloqueado = lector.GetInt32(3) == 1? true: false ;
                usuario.Intentos = lector.GetInt32(4);
            
            }

            lector.Close();

            comando.Parameters.Clear();
            comando.Dispose();

            Cerrar();

            return usuario;
        }

        public int Escribir(string Sql, List<SqlParameter> parametros)
        {
            int filasAfectadas=0;
            Abrir();
            SqlCommand comando = CrearComando(Sql, parametros);

            try
            {
                filasAfectadas =comando.ExecuteNonQuery();
            }
            catch
            {

                filasAfectadas = -1;
            }

            comando.Parameters.Clear();
            comando.Dispose();
            Cerrar();
            return filasAfectadas;
                
        }

        public int LeerEscalar(string sql, List<SqlParameter> parametros = null)
        {
            Abrir();
            SqlCommand comando = CrearComando(sql, parametros);

            int res =int.Parse( comando.ExecuteScalar().ToString());

            comando.Parameters.Clear();
            comando.Dispose();
            Cerrar();
            
            return res;
        }


    }
}
